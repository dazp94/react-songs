import React from "react";
import { connect } from "react-redux";
import { selectSong } from "../actions";

class SongDisplay extends React.Component {
  constructor(props) {
    super(props);
    this.song = "Selected song: ";
  }
  render() {
    console.log(this.props);
    return (
      <div className="ui">
        {this.song} {this.props.selectedSong.title}
      </div>
    );
  }
}

const mapStateToProps = state => {
  //console.log("SongDisplay", state);
  return state;
};

export default connect(
  mapStateToProps,
  { selectSong }
)(SongDisplay);
