import React from "react";
import { connect } from "react-redux";
import { selectSong } from "../actions";
import SongDisplay from "./SongDisplay";

class SongList extends React.Component {
  buildComponent() {
    return (
      <div>
        <div className="ui divived list">{this.renderList()}</div>
        {/*
        <SongDisplay />
        */}
      </div>
    );
  }

  onClickListener(song) {
    this.props.selectSong(song);
  }

  renderList() {
    return this.props.song.map(song => {
      return (
        <div className="item" key={song.title}>
          <div className="right floated content">
            <button
              className="ui button primary"
              onClick={() => this.onClickListener(song)}
            >
              Select
            </button>
          </div>
          <div className="content">{song.title}</div>
        </div>
      );
    });
  }

  render() {
    console.log(this.props);
    return this.buildComponent();
  }
}

const mapStateToProps = state => {
  console.log("SongList", state);

  return state;
};

export default connect(
  mapStateToProps,
  { selectSong }
)(SongList);
